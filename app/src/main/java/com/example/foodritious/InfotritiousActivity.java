package com.example.foodritious;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

public class InfotritiousActivity extends AppCompatActivity {
    GridLayout textfood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infotritious);

        textfood=(GridLayout)findViewById(R.id.mainGrid);

        setSingleEvent(textfood);
    }

    private void setSingleEvent(GridLayout textfood) {
        for(int i = 0; i<textfood.getChildCount();i++)
        {
            CardView cardView = (CardView) textfood.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(finalI==0){
                        Intent intent = new Intent(InfotritiousActivity.this, ReciousActivity.class);
                        startActivity(intent);
                    }
                    if(finalI==1){
                        Intent intent = new Intent(InfotritiousActivity.this, CooktritiousActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(InfotritiousActivity.this, "Clicked at the features " + finalI,
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.id_use) {
            Intent intent = new Intent(InfotritiousActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.id_settings) {
            return true;
        }
        if (id == R.id.id_out) {
            Intent intent = new Intent(InfotritiousActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();

        }
        return true;
    }
}
