package com.example.foodritious;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    GridLayout textfood;
    CallbackManager callbackManager;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textfood=(GridLayout)findViewById(R.id.mainGrid);
        mAuth = FirebaseAuth.getInstance();
        setSingleEvent(textfood);
        callbackManager = CallbackManager.Factory.create();
    }

    private void setSingleEvent(GridLayout textfood) {
        for(int i = 0; i<textfood.getChildCount();i++)
        {
            CardView cardView = (CardView) textfood.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(finalI==0){
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                    }else if(finalI==1){
                        Intent intent = new Intent(MainActivity.this, InfotritiousActivity.class);
                        startActivity(intent);
                    }else if(finalI==2){
                        Intent intent = new Intent(MainActivity.this, GetritiousActivity.class);
                        startActivity(intent);
                    }
                    else if(finalI==3){
                        Intent intent = new Intent(MainActivity.this, CalculitritiousActivity.class);
                        startActivity(intent);
                    }
                    else if(finalI==4){
//                        Intent intent = new Intent(MainActivity.this, CalculitritiousActivity.class);
//                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(MainActivity.this, FeedbackActivity.class);
                        startActivity(intent);
//                        Toast.makeText(MainActivity.this, "Clicked at the features " + finalI,
//                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.id_use) {
            Intent intent = new Intent(MainActivity.this, UsingAppsActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.id_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));

        }
        if (id == R.id.id_out) {
            LoginManager.getInstance().logOut();
            mAuth.signOut();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }
}
