package com.example.foodritious;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DetailReciousActivity extends AppCompatActivity {

    TextView judulResep,waktuMasak,porsi,kalori, bahan, tahapan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_recious);

        judulResep = findViewById(R.id.txtnamaResep);
        waktuMasak = findViewById(R.id.txtWaktu);
        porsi = findViewById(R.id.txtPorsi);
        kalori = findViewById(R.id.txtKalori);
        bahan = findViewById(R.id.txtBahan);
        tahapan = findViewById(R.id.txtTahapan);

        Intent intent = getIntent();
        String judul = intent.getStringExtra("judulResep");
        String waktu = intent.getStringExtra("waktuMasak");
        String pors = intent.getStringExtra("porsi");
        String klr = intent.getStringExtra("kalori");
        String bhn = intent.getStringExtra("bahan");
        String thpn = intent.getStringExtra("tahapan");

        judulResep.setText(judul);
        waktuMasak.setText(waktu);
        porsi.setText(pors);
        kalori.setText(klr);
        bahan.setText(bhn);
        tahapan.setText(thpn);
    }
}
