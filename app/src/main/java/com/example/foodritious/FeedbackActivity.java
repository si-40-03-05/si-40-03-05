package com.example.foodritious;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener{
    Button buttonSubmitFeedback;
    EditText editTextNama, editTextSaran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        editTextNama = (EditText)findViewById(R.id.edit_text_nama);
        editTextSaran = (EditText)findViewById(R.id.edit_text_saran);
        buttonSubmitFeedback = (Button)findViewById(R.id.btnSubmitFeedback);
    }

    @Override
    public void onClick(View v) {
        if (v==buttonSubmitFeedback){
            Intent intent = new Intent(FeedbackActivity.this, MainActivity.class);
            Toast.makeText(FeedbackActivity.this, "Feedback Berhasil di Input", Toast.LENGTH_SHORT).show();
            startActivity(intent);
            finish();
        }
    }
}
