package com.example.foodritious;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ReciousAdapter extends RecyclerView.Adapter<ReciousAdapter.ViewHolder> {
    private ArrayList<Recious> daftarRecious;
    private Context mContext;
    private CardView cardView;

    public ReciousAdapter(ArrayList<Recious> recious, Context mContext) {
        this.daftarRecious = recious;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ReciousAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReciousAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.cardview_item_recious, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReciousAdapter.ViewHolder holder, final int position) {
        final Recious recious = daftarRecious.get(position);
        holder.bindTo(recious);

        holder.cvRecious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, DetailReciousActivity.class);
                intent.putExtra("judulResep", daftarRecious.get(position).getJudulResep());
                intent.putExtra("waktuMasak", daftarRecious.get(position).getWaktuMasak());
                intent.putExtra("porsi", daftarRecious.get(position).getPorsi());
                intent.putExtra("kalori", daftarRecious.get(position).getKalori());
                intent.putExtra("bahan", daftarRecious.get(position).getBahan());
                intent.putExtra("tahapan", daftarRecious.get(position).getTahapan());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return daftarRecious.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView judulResep,waktuMasak, porsi, kalori;
        private ImageView fotoResep;
        private CardView cvRecious;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            judulResep = itemView.findViewById(R.id.judulResep);
            waktuMasak = itemView.findViewById(R.id.waktu_masak);
            porsi = itemView.findViewById(R.id.porsi);
            kalori = itemView.findViewById(R.id.kalori);
            cvRecious = itemView.findViewById(R.id.cardView_Recious);

            fotoResep = itemView.findViewById(R.id.fotoResep);

            itemView.setOnClickListener(this);
        }

        @SuppressLint("StaticFieldLeak")
        public void bindTo(final Recious recious) {
            judulResep.setText(recious.getJudulResep());
            waktuMasak.setText(recious.getWaktuMasak());
            porsi.setText(recious.getPorsi());
            kalori.setText(recious.getKalori());

            final StorageReference islandRef = FirebaseStorage.getInstance().getReference().child("images/" + recious.getImagePath());

            final long ONE_MEGABYTE = 10* 1024 * 1024;
            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Drawable d = Drawable.createFromStream(new ByteArrayInputStream(bytes), null);
                    fotoResep.setImageDrawable(d);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    fotoResep.setImageResource(R.drawable.ic_launcher_background);
                }
            });
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, judulResep.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
