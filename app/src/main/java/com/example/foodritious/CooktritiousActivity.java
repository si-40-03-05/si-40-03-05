package com.example.foodritious;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CooktritiousActivity extends AppCompatActivity {

    List<Cookritious> lstCookritious;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cooktritious);

        lstCookritious = new ArrayList<>();
        lstCookritious.add(new Cookritious("Daging Sapi","Categorie Makanan",
                "Daging Sapi\n" +
                        "18,8 gr protein / 100gr\n" +
                        "Vitamin B1,B2,B3,B12,B6\n" +
                        "201 kkal / 100gram\n",R.drawable.dagingsapi));
        lstCookritious.add(new Cookritious("Bawang Putih","Categorie Makanan",
                "Bawang Putih\n" +
                        "6,36 gr protein / 100gr\n" +
                        "Vitamin A, C, E, K\n" +
                        "149 kkal / 100gram\n",R.drawable.bawangputih));
        lstCookritious.add(new Cookritious("Bawang Bombay","Categorie Makanan",
                "Bawang Bombay\n" +
                        "1,51 gr protein / 100gr\n" +
                        "Vitamin C, B\n" +
                        "94 kkal / 100gram\n",R.drawable.bawangbombay));
        lstCookritious.add(new Cookritious("Jahe","Categorie Makanan",
                "Jahe\n" +
                        "0,04 gr protein / 100gr\n" +
                        "Vitamin A, C, E, B\n" +
                        "2 kkal / 1 sdt\n",R.drawable.jahe));
        lstCookritious.add(new Cookritious("Cabe merah","Categorie Makanan",
                "Cabe merah\n" +
                        "0.64 gr protein / 1sdm\n" +
                        "Vitamin C\n" +
                        "17 kkal / 1 sdm\n",R.drawable.cabemerah));

        lstCookritious.add(new Cookritious("Daging Sapi","Categorie Makanan",
                "Daging Sapi\n" +
                        "18,8 gr protein / 100gr\n" +
                        "Vitamin B1,B2,B3,B12,B6\n" +
                        "201 kkal / 100gram\n",R.drawable.dagingsapi));
        lstCookritious.add(new Cookritious("Bawang Putih","Categorie Makanan",
                "Bawang Putih\n" +
                        "6,36 gr protein / 100gr\n" +
                        "Vitamin A, C, E, K\n" +
                        "149 kkal / 100gram\n",R.drawable.bawangputih));
        lstCookritious.add(new Cookritious("Bawang Bombay","Categorie Makanan",
                "Bawang Bombay\n" +
                        "1,51 gr protein / 100gr\n" +
                        "Vitamin C, B\n" +
                        "94 kkal / 100gram\n",R.drawable.bawangbombay));
        lstCookritious.add(new Cookritious("Jahe","Categorie Makanan",
                "Jahe\n" +
                        "0,04 gr protein / 100gr\n" +
                        "Vitamin A, C, E, B\n" +
                        "2 kkal / 1 sdt\n",R.drawable.jahe));
        lstCookritious.add(new Cookritious("Cabe merah","Categorie Makanan",
                "Cabe merah\n" +
                        "0.64 gr protein / 1sdm\n" +
                        "Vitamin C\n" +
                        "17 kkal / 1 sdm\n",R.drawable.cabemerah));

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_id);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, lstCookritious);
        myrv.setLayoutManager(new GridLayoutManager(this,3));
        myrv.setAdapter(myAdapter);

    }
}
