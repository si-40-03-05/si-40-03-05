package com.example.foodritious;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ResultCalculitritiousActivity extends AppCompatActivity {
    TextView txtNama,txtJk, txtBb, txtTb, txtBmi, txtHasil, txtKet, txtu, txtkal;
    String nama,jk,hasil,ket,output;
    double bb,tb,bmi, mur;
    double defaultValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_calculitritious);

        txtNama = (TextView)findViewById(R.id.text_view_hasil_nama);
        txtJk=(TextView)findViewById(R.id.text_jk);
        txtBb=(TextView)findViewById(R.id.text_view_hasil_bb);
        txtTb=(TextView)findViewById(R.id.text_view_hasil_tb);
        txtBmi=(TextView)findViewById(R.id.text_view_hasil_bmi);
        txtkal=(TextView)findViewById(R.id.text_view_hasil_kal);
        txtHasil=(TextView)findViewById(R.id.text_view_hasil);
        txtu = (TextView)findViewById(R.id.text_umur);
        txtKet=(TextView)findViewById(R.id.text_view_hasil_ket);

        //mengambil variabel dari activity lain
        Intent intent = getIntent();
        //String pesan = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        nama = intent.getStringExtra("EXTRA_NAMA");
        jk = intent.getStringExtra("EXTRA_JK");
        bb = intent.getDoubleExtra("EXTRA_BB",defaultValue);
        tb = intent.getDoubleExtra("EXTRA_TB",defaultValue);
        bmi = intent.getDoubleExtra("EXTRA_BMI",defaultValue);
        mur = intent.getDoubleExtra("EXTRA_umur",defaultValue);
        hasil = intent.getStringExtra("EXTRA_HASIL");
        ket = intent.getStringExtra("EXTRA_KET");
        output = intent.getStringExtra("EXTRA_kal");

        txtNama.setText("Nama : "+ nama);
        txtJk.setText("Jenis Kelamin : "+ jk);
        txtBb.setText("Berat Badani : "+bb);
        txtTb.setText("Tinggi Badan : "+tb);
        txtBmi.setText("BMI : "+bmi);
        txtHasil.setText("Hasil : "+hasil);
        txtu.setText("Umur :" +mur);
        txtKet.setText("Keterangan : "+ket);
        txtkal.setText("Kalori :" +output);
    }

    public void bagikanKeEmail(View view) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:")); // only email apps should handle this
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Laporan Berat Badan "+nama);
        emailIntent.putExtra(Intent.EXTRA_TEXT,"Nama : "+nama+"\nJenis Kelamin : "+jk+"\nBerat Badan : "+bb+"\nUmur: "+mur+"\nTinggi Badan : "+tb+"\nBMI : "
                +bmi+"\nHasil : "+hasil+"\nKeterangan : "+ket+"\nKalori : "+output );
        if (emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(emailIntent);
        }
    }
}
