package com.example.foodritious;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class GetririousViewAdapter extends RecyclerView.Adapter<GetririousViewAdapter.ViewHolder>{

    private static final String TAG = "GetritiousAdapter";

    //var
    private ArrayList<String> nPaket = new ArrayList<>();
    private ArrayList<String> nDeskrispi = new ArrayList<>();
    private ArrayList<String> nHarga = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private Context mContext;
    private CardView cardView;

    public GetririousViewAdapter(Context context, ArrayList<String> nPaket, ArrayList<String> nDeskrispi, ArrayList<String> nHarga, ArrayList<String> mImageUrls) {
        this.nPaket = nPaket;
        this.nDeskrispi = nDeskrispi;
        this.nHarga = nHarga;
        this.mImageUrls = mImageUrls;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d(TAG,"onCreateViewHolder, called");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.getritious_listener, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,final int i) {
        Log.d(TAG,"onBindViewHolder, called");

        Glide.with(mContext)
                .asBitmap()
                .load(mImageUrls.get(i))
                .into(viewHolder.imagePaket);

        viewHolder.textViewNamaPaket.setText(nPaket.get(i));
        viewHolder.imagePaket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"onClick, Clicked name " +nPaket.get(i));
                Toast.makeText(mContext,nPaket.get(i),Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.cardViewGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, PaymentActivity.class);
                intent.putExtra("judulPaket", "Paket :" +nPaket.get(i));
                intent.putExtra("harga", "Harga :"+nHarga.get(i));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imagePaket;
        TextView textViewNamaPaket, textViewDeskripsi, textViewHarga;
        CardView cardViewGet;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagePaket = itemView.findViewById(R.id.gambar);
            textViewNamaPaket = itemView.findViewById(R.id.titlePaket);
            textViewDeskripsi = itemView.findViewById(R.id.deskripsi);
            textViewHarga = itemView.findViewById(R.id.harga);
            cardViewGet = itemView.findViewById(R.id.cvGet);
        }
    }
}
