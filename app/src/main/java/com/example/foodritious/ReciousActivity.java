package com.example.foodritious;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ProgressBar;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ReciousActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Recious> recious;
    ReciousAdapter reciousAdapter;
    FirebaseFirestore db;
    FirebaseAuth mAuth;
    ProgressBar progressBar;
    int REQUEST_MENU = 404;
    FloatingActionButton fab;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recious);

        mAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressbarRecious);
        fab = findViewById(R.id.fab);
        String userID = mAuth.getUid();

        Log.d("MANTAPPP", "onCreate: " +userID);

        if (userID == "FFTatEik6FWB0GzwdKSDuaw8ZxD3" && fab.getVisibility() == View.VISIBLE){
            fab.show();
        }else if (fab.getVisibility() != View.VISIBLE){
            fab.hide();
        }

        recyclerView = findViewById(R.id.rvMain);
        recious = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(ReciousActivity.this));
        reciousAdapter = new ReciousAdapter(recious, ReciousActivity.this);
        recyclerView.setAdapter(reciousAdapter);
        db = FirebaseFirestore.getInstance();
        init();
    }

    public void add(View view) {
        startActivityForResult(new Intent(ReciousActivity.this, InputReciousActivity.class), REQUEST_MENU);
    }

    @SuppressLint("StaticFieldLeak")
    public void init() {
        recious.clear();
        progressBar.setVisibility(View.VISIBLE);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                getthat();
                return null;
            }
        }.execute();

    }


    public void loadMenu(View view) {
        init();
    }

    private void getthat() {
        db.collection(mAuth.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d("Result", document.getId() + " => " + document.getData());
                        recious.add(new Recious(document.getId(), document.get("JudulResep").toString(), document.get("WaktuMasak").toString(), document.get("Porsi").toString(), document.get("Kalori").toString(), document.get("Bahan").toString(), document.get("Tahapan").toString()));
                    }
                    reciousAdapter.notifyDataSetChanged();

                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        init();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.id_use) {
            Intent intent = new Intent(ReciousActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.id_settings) {
            startActivity(new Intent(ReciousActivity.this, SettingsActivity.class));

        }
        if (id == R.id.id_out) {
            LoginManager.getInstance().logOut();
            mAuth.signOut();
            Intent intent = new Intent(ReciousActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }
}
