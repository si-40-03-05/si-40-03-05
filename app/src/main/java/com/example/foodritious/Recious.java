package com.example.foodritious;

public class Recious {
    String imagePath,judulResep,waktuMasak,porsi,kalori, bahan, tahapan;

    public Recious(String imagePath, String judulResep, String waktuMasak, String porsi, String kalori, String bahan, String tahapan) {
        this.imagePath = imagePath;
        this.judulResep = judulResep;
        this.waktuMasak = waktuMasak;
        this.porsi = porsi;
        this.kalori = kalori;
        this.bahan = bahan;
        this.tahapan = tahapan;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getJudulResep() {
        return judulResep;
    }

    public String getWaktuMasak() {
        return waktuMasak;
    }

    public String getPorsi() {
        return porsi;
    }

    public String getKalori() {
        return kalori;
    }

    public String getBahan() {
        return bahan;
    }

    public String getTahapan() {
        return tahapan;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setJudulResep(String judulResep) {
        this.judulResep = judulResep;
    }

    public void setWaktuMasak(String waktuMasak) {
        this.waktuMasak = waktuMasak;
    }

    public void setPorsi(String porsi) {
        this.porsi = porsi;
    }

    public void setKalori(String kalori) {
        this.kalori = kalori;
    }

    public void setBahan(String bahan) {
        this.bahan = bahan;
    }

    public void setTahapan(String tahapan) {
        this.tahapan = tahapan;
    }
}
